<?php
require_once 'inc/config.php';
require_once 'inc/functions.php';

$g_api_key = $config['g_api_key'];
$docsValidation['images'] = [
	'image/jpeg',//0
	'image/png',//1
	'image/gif',//2
];
$docsValidation['pdfs'] = [
	'application/pdfFile',//3
	'application/pdf',//4
];
if($_FILES) {
	if(!empty($_FILES['doc']['type'])) {
    	$doc = file_get_contents($_FILES['doc']['tmp_name']);
		
		$detectionType = 'DOCUMENT_TEXT_DETECTION';
		// $detectionType = 'TEXT_DETECTION';
		if (in_array($_FILES['doc']['type'], $docsValidation['pdfs'])) { // if application/pdfFile
			$urlBase = 'https://vision.googleapis.com/v1/files:asyncBatchAnnotate';
			$obj = ''; // needs to be changed
		} else if (in_array($_FILES['doc']['type'], $docsValidation['images'])) { // if images
			$urlBase = 'https://vision.googleapis.com/v1/images:annotate';
			$imageBase64 = base64_encode($doc);
			$obj = '"image": {
			    "content":"' . $imageBase64. '"
			},';
		}

		$jsonRequest ='{
		  	"requests": [
				{
					'.$obj.'
					"features": [
					    {
					      	"type": "' .$detectionType. '",
					      	"maxResults": 10,
					    }
					]
				}
			]
		}';

		$url = $urlBase."?key={$g_api_key}";

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonRequest);
		$jsonResponse = curl_exec($curl);
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		
		if ( $status != 200 ) {
			die("Something when wrong. Status code: $status" );
		}
		
		// switch ($_FILES['doc']['type']) {
		// 	case 'image/jpeg':
		// 		$im = imagecreatefromjpeg($_FILES['doc']['tmp_name']);
		// 		break;
		// 	case 'image/png':
		// 		$im = imagecreatefrompng($_FILES['doc']['tmp_name']);
		// 		break;
		// 	case 'image/gif':
		// 		$im = imagecreatefromgif($_FILES['doc']['tmp_name']);
		// 		break;
		// }

		$response = json_decode($jsonResponse, true);			
		
		$text = isset($response['responses'][0]['textAnnotations'][0]['description'])
					?$response['responses'][0]['textAnnotations'][0]['description']:'';

		// echo'<pre>';
		// print_r($response['responses'][0]['textAnnotations'][0]['description']);
		// echo'</pre>';
	
    } else{
    	echo "{$_FILES['doc']['type']} File type not allowed";
    }
}

if (!empty($text)) {
	// echo $text;
	$data=[];
	foreach ($config['forms']['pcaa'] as $key => $val) {
		// dd($val);
		$name = get_string_between($text, $val['sk'], $val['ek']);
		// if (!empty($name)) {
			$data[$key] = trim($name); 
		// }
	}

	dd($data);
}

?>


<!DOCTYPE html>
<html>
<head>
	<title>Implementation of Google Vision API by: M.Shamiq Hussain (TheDev)</title>
</head>
<body>

	<form enctype="multipart/form-data" action="" method="POST">
		Choose an image to upload: <input name="doc" type="file" /><br />
		<input type="submit" value="Upload Document" />
	</form>
</body>
</html>
